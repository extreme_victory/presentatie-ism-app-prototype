package me.jasper.securityapp;

/**
 *
 * @author Jasper Stath
 */
public class MainMenu extends javax.swing.JFrame {
    
	private static final long serialVersionUID = 2008323488047604852L;

    public MainMenu() {
        initComponents();
    }
    
    private void initComponents() {

        topText2 = new javax.swing.JLabel();
        topText = new javax.swing.JLabel();
        topText1 = new javax.swing.JLabel();
        btn_top = new javax.swing.JButton();
        btn_down = new javax.swing.JButton();
        btn_middle = new javax.swing.JButton();
        header = new javax.swing.JLabel();
        background = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Bewust!");
        setLocation(new java.awt.Point(800, 120));
        setMinimumSize(new java.awt.Dimension(480, 850));
        setResizable(false);
        getContentPane().setLayout(null);

        topText2.setFont(new java.awt.Font("Trebuchet MS", 0, 48)); // NOI18N
        topText2.setForeground(new java.awt.Color(0, 127, 127));
        topText2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        topText2.setText("Wachtwoordkraker");
        topText2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        getContentPane().add(topText2);
        topText2.setBounds(40, 690, 410, 100);

        topText.setFont(new java.awt.Font("Trebuchet MS", 0, 48)); // NOI18N
        topText.setForeground(new java.awt.Color(0, 127, 127));
        topText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        topText.setText("Wachtwoordmaker");
        topText.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        getContentPane().add(topText);
        topText.setBounds(30, 550, 420, 100);

        topText1.setFont(new java.awt.Font("Trebuchet MS", 0, 48)); // NOI18N
        topText1.setForeground(new java.awt.Color(0, 127, 127));
        topText1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        topText1.setText("Speel het spel!");
        topText1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        getContentPane().add(topText1);
        topText1.setBounds(50, 410, 390, 100);

        btn_top.setIcon(new javax.swing.ImageIcon(getClass().getResource("/securityapp/btn.png"))); // NOI18N
        btn_top.setBorder(null);
        btn_top.setBorderPainted(false);
        btn_top.setContentAreaFilled(false);
        btn_top.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_top.setFocusPainted(false);
        getContentPane().add(btn_top);
        btn_top.setBounds(10, 390, 460, 140);

        btn_down.setIcon(new javax.swing.ImageIcon(getClass().getResource("/securityapp/btn.png"))); // NOI18N
        btn_down.setBorder(null);
        btn_down.setBorderPainted(false);
        btn_down.setContentAreaFilled(false);
        btn_down.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_down.setFocusPainted(false);
        btn_down.addActionListener(e -> btn_downActionPerformed(e));
        getContentPane().add(btn_down);
        btn_down.setBounds(10, 670, 460, 140);

        btn_middle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/securityapp/btn.png"))); // NOI18N
        btn_middle.setBorder(null);
        btn_middle.setBorderPainted(false);
        btn_middle.setContentAreaFilled(false);
        btn_middle.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_middle.setFocusPainted(false);
        btn_middle.addActionListener(e -> btn_middleActionPerformed(e));
        getContentPane().add(btn_middle);
        btn_middle.setBounds(10, 530, 460, 140);

        header.setBackground(new java.awt.Color(0, 127, 127));
        header.setFont(new java.awt.Font("Chiller", 0, 96)); // NOI18N
        header.setForeground(new java.awt.Color(255, 255, 255));
        header.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        header.setText("Bewust!");
        header.setOpaque(true);
        getContentPane().add(header);
        header.setBounds(0, 0, 480, 140);

        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/securityapp/app_bg.png"))); // NOI18N
        getContentPane().add(background);
        background.setBounds(-70, 130, 610, 730);

        pack();
    }                                                             

    private void btn_middleActionPerformed(java.awt.event.ActionEvent evt) {                                           
        PasswordMaker maker = new PasswordMaker();
        maker.setVisible(true);
        
        this.dispose();
    }                                          

    private void btn_downActionPerformed(java.awt.event.ActionEvent evt) {                                         
        PasswordTester tester = new PasswordTester();
        tester.setVisible(true);
        
        this.dispose();
    }                                        

    // Variables declaration - do not modify                     
    private javax.swing.JLabel background;
    private javax.swing.JButton btn_down;
    private javax.swing.JButton btn_middle;
    private javax.swing.JButton btn_top;
    private javax.swing.JLabel header;
    private javax.swing.JLabel topText;
    private javax.swing.JLabel topText1;
    private javax.swing.JLabel topText2;
    // End of variables declaration                   
}
