package me.jasper.securityapp;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.util.Random;
import java.util.stream.Collectors;

/**
 *
 * @author Jasper
 */
public class PasswordMaker extends javax.swing.JFrame {

	private static final long serialVersionUID = -1272288279664304985L;

    public PasswordMaker() {
        initComponents();
    }
                          
    private void initComponents() {

        btn_back = new javax.swing.JButton();
        header = new javax.swing.JLabel();
        titleUp = new javax.swing.JLabel();
        tekenSlider = new javax.swing.JSlider();
        aantalTekens = new javax.swing.JLabel();
        titleDown = new javax.swing.JLabel();
        btn_generate = new javax.swing.JButton();
        wachtwoordIs = new javax.swing.JLabel();
        wachtwoord = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Bewust!");
        setBackground(new java.awt.Color(153, 0, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setLocation(new java.awt.Point(800, 120));
        setMinimumSize(new java.awt.Dimension(480, 850));
        setResizable(false);
        getContentPane().setLayout(null);

        btn_back.setFont(new java.awt.Font("Times New Roman", 0, 48)); // NOI18N
        btn_back.setText("←");
        btn_back.setBorder(null);
        btn_back.setBorderPainted(false);
        btn_back.setContentAreaFilled(false);
        btn_back.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_back.setFocusPainted(false);
        btn_back.addActionListener(e -> btn_backActionPerformed(e));
        getContentPane().add(btn_back);
        btn_back.setBounds(20, 40, 50, 50);

        header.setBackground(new java.awt.Color(0, 127, 127));
        header.setFont(new java.awt.Font("Chiller", 0, 96)); // NOI18N
        header.setForeground(new java.awt.Color(255, 255, 255));
        header.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        header.setText("Bewust!");
        header.setOpaque(true);
        getContentPane().add(header);
        header.setBounds(0, 0, 480, 140);

        titleUp.setFont(new java.awt.Font("Trebuchet MS", 0, 36)); // NOI18N
        titleUp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleUp.setText("Wachtwoord Generator");
        getContentPane().add(titleUp);
        titleUp.setBounds(10, 160, 450, 60);

        tekenSlider.setMajorTickSpacing(5);
        tekenSlider.setMaximum(25);
        tekenSlider.setMinimum(10);
        tekenSlider.setMinorTickSpacing(1);
        tekenSlider.setPaintLabels(true);
        tekenSlider.setPaintTicks(true);
        tekenSlider.setValue(10);
        getContentPane().add(tekenSlider);
        tekenSlider.setBounds(50, 410, 370, 50);

        aantalTekens.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        aantalTekens.setForeground(new java.awt.Color(0, 127, 127));
        aantalTekens.setText("Aantal tekens:");
        getContentPane().add(aantalTekens);
        aantalTekens.setBounds(70, 380, 130, 30);

        titleDown.setFont(new java.awt.Font("Trebuchet MS", 0, 24)); // NOI18N
        titleDown.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleDown.setText("Genereer jouw eigen unieke wachtwoord!");
        getContentPane().add(titleDown);
        titleDown.setBounds(0, 230, 480, 30);

        btn_generate.setFont(new java.awt.Font("Trebuchet MS", 0, 36)); // NOI18N
        btn_generate.setForeground(new java.awt.Color(0, 127, 127));
        btn_generate.setText("Genereer nu!");
        btn_generate.setBorder(null);
        btn_generate.setBorderPainted(false);
        btn_generate.setContentAreaFilled(false);
        btn_generate.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_generate.setFocusPainted(false);
        btn_generate.addActionListener(e -> btn_generateActionPerformed(e));
        getContentPane().add(btn_generate);
        btn_generate.setBounds(80, 680, 330, 90);

        wachtwoordIs.setFont(new java.awt.Font("Trebuchet MS", 0, 24)); // NOI18N
        wachtwoordIs.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(wachtwoordIs);
        wachtwoordIs.setBounds(120, 530, 240, 60);

        wachtwoord.setFont(new java.awt.Font("Trebuchet MS", 0, 24)); // NOI18N
        wachtwoord.setForeground(new java.awt.Color(0, 127, 127));
        wachtwoord.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(wachtwoord);
        wachtwoord.setBounds(50, 590, 390, 60);

        pack();
    }                       

    private void btn_generateActionPerformed(java.awt.event.ActionEvent evt) {                                             
        String password = generatePassword(tekenSlider.getValue());
        wachtwoord.setText(password);
        wachtwoordIs.setText("Jouw wachtwoord is:");
        
        StringSelection selection = new StringSelection(password);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
        
    }                                            

    private void btn_backActionPerformed(java.awt.event.ActionEvent evt) {                                         
        this.dispose();
        
        MainMenu menu = new MainMenu();
        menu.setVisible(true);
    }                                        

    public String generatePassword(int size) {
        return new Random().ints(size, 33, 122).mapToObj(i -> String.valueOf((char)i)).collect(Collectors.joining());
    }

    private javax.swing.JLabel aantalTekens;
    private javax.swing.JButton btn_back;
    private javax.swing.JButton btn_generate;
    private javax.swing.JLabel header;
    private javax.swing.JSlider tekenSlider;
    private javax.swing.JLabel titleDown;
    private javax.swing.JLabel titleUp;
    private javax.swing.JLabel wachtwoord;
    private javax.swing.JLabel wachtwoordIs;
}
