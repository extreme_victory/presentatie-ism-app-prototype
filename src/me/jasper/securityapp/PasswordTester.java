package me.jasper.securityapp;

public class PasswordTester extends javax.swing.JFrame {

	private static final long serialVersionUID = -2344578857913799698L;

	public PasswordTester() {
		initComponents();
	}

	private void initComponents() {

		btn_back = new javax.swing.JButton();
		header = new javax.swing.JLabel();
		titleUp = new javax.swing.JLabel();
		titleDown = new javax.swing.JLabel();
		btn_test = new javax.swing.JButton();
		aantalTekens = new javax.swing.JLabel();
		passwordField = new javax.swing.JTextField();
		spaceWarning = new javax.swing.JLabel();
		passwordTip1 = new javax.swing.JLabel();
		passwordTip2 = new javax.swing.JLabel();
		passwordTip3 = new javax.swing.JLabel();
		passwordTip4 = new javax.swing.JLabel();
		passwordTip5 = new javax.swing.JLabel();
		passwordInfo = new javax.swing.JLabel();
		passwordTips = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Bewust!");
		setBackground(new java.awt.Color(153, 0, 255));
		setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		setLocation(new java.awt.Point(800, 120));
		setMinimumSize(new java.awt.Dimension(480, 850));
		setResizable(false);
		getContentPane().setLayout(null);

		btn_back.setFont(new java.awt.Font("Times New Roman", 0, 48)); 
		btn_back.setText("←");
		btn_back.setBorder(null);
		btn_back.setBorderPainted(false);
		btn_back.setContentAreaFilled(false);
		btn_back.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		btn_back.setFocusPainted(false);
		btn_back.addActionListener(e -> btn_backActionPerformed(e));
		getContentPane().add(btn_back);
		btn_back.setBounds(20, 40, 50, 50);

		header.setBackground(new java.awt.Color(0, 127, 127));
		header.setFont(new java.awt.Font("Chiller", 0, 96)); 
		header.setForeground(new java.awt.Color(255, 255, 255));
		header.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		header.setText("Bewust!");
		header.setOpaque(true);
		getContentPane().add(header);
		header.setBounds(0, 0, 480, 140);

		titleUp.setFont(new java.awt.Font("Trebuchet MS", 0, 36)); // NOI18N
		titleUp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		titleUp.setText("Wachtwoord Tester");
		getContentPane().add(titleUp);
		titleUp.setBounds(10, 160, 450, 60);

		titleDown.setFont(new java.awt.Font("Trebuchet MS", 0, 24)); // NOI18N
		titleDown.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		titleDown.setText("Test de kracht van jouw wachtwoord!");
		getContentPane().add(titleDown);
		titleDown.setBounds(0, 230, 480, 30);

		btn_test.setFont(new java.awt.Font("Trebuchet MS", 0, 36)); // NOI18N
		btn_test.setForeground(new java.awt.Color(0, 127, 127));
		btn_test.setText("Test nu!");
		btn_test.setBorder(null);
		btn_test.setBorderPainted(false);
		btn_test.setContentAreaFilled(false);
		btn_test.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		btn_test.setFocusPainted(false);
		btn_test.addActionListener(e -> btn_testActionPerformed(e));
		getContentPane().add(btn_test);
		btn_test.setBounds(80, 680, 330, 90);

		aantalTekens.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
		aantalTekens.setForeground(new java.awt.Color(0, 127, 127));
		aantalTekens.setText("Jouw wachtwoord:");
		getContentPane().add(aantalTekens);
		aantalTekens.setBounds(70, 320, 230, 30);
		getContentPane().add(passwordField);
		passwordField.setBounds(60, 350, 380, 30);

		spaceWarning.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
		spaceWarning.setForeground(new java.awt.Color(255, 0, 0));
		spaceWarning.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		getContentPane().add(spaceWarning);
		spaceWarning.setBounds(120, 380, 260, 30);

		passwordTip1.setFont(new java.awt.Font("Trebuchet MS", 0, 18)); // NOI18N
		getContentPane().add(passwordTip1);
		passwordTip1.setBounds(60, 480, 410, 30);

		passwordTip2.setFont(new java.awt.Font("Trebuchet MS", 0, 18)); // NOI18N
		getContentPane().add(passwordTip2);
		passwordTip2.setBounds(60, 510, 410, 30);

		passwordTip3.setFont(new java.awt.Font("Trebuchet MS", 0, 18)); // NOI18N
		getContentPane().add(passwordTip3);
		passwordTip3.setBounds(60, 540, 410, 30);

		passwordTip4.setFont(new java.awt.Font("Trebuchet MS", 0, 18)); // NOI18N
		getContentPane().add(passwordTip4);
		passwordTip4.setBounds(60, 570, 410, 30);

		passwordTip5.setFont(new java.awt.Font("Trebuchet MS", 0, 18)); // NOI18N
		getContentPane().add(passwordTip5);
		passwordTip5.setBounds(60, 600, 410, 30);

		passwordInfo.setFont(new java.awt.Font("Trebuchet MS", 0, 24)); // NOI18N
		passwordInfo.setForeground(new java.awt.Color(0, 127, 127));
		passwordInfo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		getContentPane().add(passwordInfo);
		passwordInfo.setBounds(20, 390, 440, 30);

		passwordTips.setFont(new java.awt.Font("Trebuchet MS", 0, 18)); // NOI18N
		passwordTips.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		getContentPane().add(passwordTips);
		passwordTips.setBounds(70, 420, 340, 20);

		pack();
	}

	private void btn_testActionPerformed(java.awt.event.ActionEvent evt) {
		passwordTip1.setText("");
		passwordTip2.setText("");
		passwordTip3.setText("");
		passwordTip4.setText("");
		passwordTip5.setText("");

		spaceWarning.setText("");
		passwordInfo.setText("");
		passwordTips.setText("");

		String password = passwordField.getText().trim();

		if (password.contains(" ")) {
			spaceWarning.setText("Wachtwoorden hebben geen spatie!");
			return;
		}

		boolean hasNumber = false;
		boolean hasUppercase = !password.equals(password.toLowerCase());
		boolean hasLowercase = !password.equals(password.toUpperCase());
		boolean hasSpecial = !password.matches("[A-Za-z0-9 ]*");
		int warnings = 0;

		if (password.length() < 8) {
			setWarning(warnings++, "* Jouw wachtwoord is te kort.");
		}

		for (int i = 0; i < 10; i++) {
			if (password.contains(i + ""))
				hasNumber = true;
		}

		if (!hasNumber) {
			setWarning(warnings++, "* Jouw wachtwoord bevat geen cijfer.");
		}

		if (!hasUppercase) {
			setWarning(warnings++, "* Jouw wachtwoord bevat geen hoofdletter.");
		}

		if (!hasLowercase) {
			setWarning(warnings++, "* Jouw wachtwoord bevat geen kleine letter.");
		}

		if (!hasSpecial) {
			setWarning(warnings++, "* Jouw wachtwoord bevat geen speciale tekens.");
		}

		setScore(warnings);

	}

	private void btn_backActionPerformed(java.awt.event.ActionEvent evt) {
		this.dispose();

		MainMenu menu = new MainMenu();
		menu.setVisible(true);
	}

	public void setScore(int warnings) {
		switch (warnings) {
		case 0:
			passwordInfo.setText("Jouw wachtwoord is perfect.");
			passwordTips.setText("");
			break;
		case 1:
			passwordInfo.setText("Jouw wachtwoord is in orde");
			passwordTips.setText("Zo kan jij je wachtwoord nog verbeteren");
			break;
		case 2:
			passwordInfo.setText("Jouw wachtwoord is zwakjes");
			passwordTips.setText("Zo kun jij je wachtwoord verbeteren");
			break;
		case 3:
			passwordInfo.setText("Jouw wachtwoord is zwak");
			passwordTips.setText("Zo kun jij je wachtwoord verbeteren");
			break;
		case 4:
			passwordInfo.setText("Jouw wachtwoord is erg zwak");
			passwordTips.setText("Zo kun jij je wachtwoord verbeteren");
			break;
		case 5:
			passwordInfo.setText("Jouw wachtwoord is erg zwak");
			passwordTips.setText("Dit heeft jouw wachtwoord nodig");
			break;
		}
	}

	public void setWarning(int row, String warning) {
		switch (row) {
		case 1:
			passwordTip1.setText(warning);
			break;
		case 2:
			passwordTip2.setText(warning);
			break;
		case 3:
			passwordTip3.setText(warning);
			break;
		case 4:
			passwordTip4.setText(warning);
			break;
		case 5:
			passwordTip5.setText(warning);
			break;
		}
	}

	private javax.swing.JLabel aantalTekens;
	private javax.swing.JButton btn_back;
	private javax.swing.JButton btn_test;
	private javax.swing.JLabel header;
	private javax.swing.JTextField passwordField;
	private javax.swing.JLabel passwordInfo;
	private javax.swing.JLabel passwordTip1;
	private javax.swing.JLabel passwordTip2;
	private javax.swing.JLabel passwordTip3;
	private javax.swing.JLabel passwordTip4;
	private javax.swing.JLabel passwordTip5;
	private javax.swing.JLabel passwordTips;
	private javax.swing.JLabel spaceWarning;
	private javax.swing.JLabel titleDown;
	private javax.swing.JLabel titleUp;
}
